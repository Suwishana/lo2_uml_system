package vehicle;

public class Car extends FuelVehicle {
	private int nbSeats;
	
	public Car(double basefee,double nbkms,double nbSeats) {
		super(basefee,nbkms);
		this.nbSeats=(int)nbSeats;
	}
	@Override
    public double getMileageFees() {
		return super.getMileageFees();
	}
	public int getNbSeats() {
		return nbSeats;
	}
	public void setNbSeats(int nbSeats) {
		this.nbSeats=nbSeats;
	}
	public double getCost() {
		return (nbSeats*getbasefee())+ getMileageFees();
		
	}
	

}
