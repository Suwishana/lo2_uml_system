package vehicle;

public class Bicycle extends RentedVehicle  {
	private int nbDays;
	
	public Bicycle(double baseFees,int nbDays) {
		super(baseFees);
		this.nbDays=nbDays;
	}
	public Bicycle(double baseFees) {
		super(baseFees);
	}
	public int getNbDays() {
		return nbDays;
	}
	public void setNbDays(int nbDays) {
		this.nbDays=nbDays;
	}
	public double getCost() {
		return nbDays*getbasefee();
	}

}
